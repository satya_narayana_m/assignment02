/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package storeLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Satyanarayana Madala
 */
public class GroceryStore {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scan=new Scanner(new File("products.txt "));
        //Scanner prodScan=new Scanner(new File("moreProducts.txt"));
        ArrayList<Product> allproducts=new ArrayList<>();
        Store store ;
        Product product;
        int numberOfProducts=0;
        int numberOfProductsPerAisle=0;
        if (scan.hasNext()) {
            numberOfProducts=scan.nextInt();
            numberOfProductsPerAisle=scan.nextInt();
            
            
            scan.nextLine();
            while (scan.hasNext()) {
                product=new Product(scan.nextLine());
                allproducts.add(product);
            }
            
        }
        store=new Store(numberOfProducts, numberOfProductsPerAisle);
        System.out.println("Printing Store for First time");
store.fillStore(allproducts);
store.printStore();
        System.out.println("Number of products in the store is: "+store.getNumberOfProducts());
        System.out.println("Number of Products per Aisle is: "+store.getNumberOfProductsPerAisle());
        System.out.println("Number of Aisles is: "+store.getNumberOfAisles());
        System.out.println("Number of rows is: "+store.getNumberOfRows());
        System.out.println("");
        scan=new Scanner(new File("moreProducts.txt"));
        while (scan.hasNext()) {
        product=new Product(scan.nextLine());
        store.addProductToStore(product);
        System.out.println("Adding "+product+" to the Store");
        store.printStore();
            System.out.println("");
        }
        System.out.println("Number of products in the store is: "+store.getNumberOfProducts());
        System.out.println("Number of Products per Aisle is: "+store.getNumberOfProductsPerAisle());
        System.out.println("Number of Aisles is: "+store.getNumberOfAisles());
        System.out.println("Number of rows is: "+store.getNumberOfRows());
        System.out.println("");
         
    }
}
