/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storeLayout;

import java.util.ArrayList;

/**
 *
 * @author Satyanaryana Madala
 */
public class Store {

    private ArrayList<ArrayList<Product>> store;
    private int numberOfProducts;
    private int numberOfProductsPerAisle;
    private int numberOfAisles;
    private int numberOfRows;

    public Store(int numberOfProducts, int numberOfProductsPerAisle) {
        this.numberOfProducts = numberOfProducts;
        this.numberOfProductsPerAisle = numberOfProductsPerAisle;
        this.store = new ArrayList<>();
        this.numberOfAisles = (int) Math.ceil(1.0 * numberOfProducts / numberOfProductsPerAisle);
        numberOfRows = 2;
        calculateNumberOfRows();
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public int getNumberOfProductsPerAisle() {
        return numberOfProductsPerAisle;
    }

    public void setNumberOfProductsPerAisle(int numberOfProductsPerAisle) {
        this.numberOfProductsPerAisle = numberOfProductsPerAisle;
    }

    public int getNumberOfAisles() {
        return numberOfAisles;
    }

    public void setNumberOfAisles(int numberOfAisles) {
        this.numberOfAisles = numberOfAisles;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public void setNumberOfRows(int numberOfRows) {
        this.numberOfRows = numberOfRows;
    }

    private void calculateNumberOfRows() {
        for (int aisle = 2; aisle <= numberOfAisles; aisle++) {
            if (aisle % 2 == 0) {
                numberOfRows += 1;
            } else {
                numberOfRows += 2;
            }
        }
    }

    public void fillStore(ArrayList<Product> allProducts) {
        int count = 0;
        ArrayList<Product> aisleProduct = null;
        for (int i = 1; i <= numberOfRows; i++) {
            if (i % 3 == 2) {
                store.add(null);
            } else {
                aisleProduct = new ArrayList<>();
                for (int j = 0; j < numberOfProductsPerAisle && count < allProducts.size(); j++, count++) {
                    aisleProduct.add(allProducts.get(count));
                }
                store.add(aisleProduct);
            }
        }
    }

    public void addProductToStore(Product p) {
//        ArrayList<Product> tempProd = new ArrayList<>();
//        for (ArrayList<Product> eachRow : store) {
//            if (null != eachRow) {
//                for (Product product : eachRow) {
//                    tempProd.add(product);
//                }
//            }
//        }
//        tempProd.add(p);
//        numberOfProducts += 1;
//        numberOfAisles = (int) Math.ceil(1.0 * numberOfProducts / numberOfProductsPerAisle);
//        numberOfRows = 2;
//        calculateNumberOfRows();
//        store = new ArrayList<>();
//        fillStore(tempProd);

        if (numberOfProducts % numberOfProductsPerAisle != 0) {
            if (numberOfRows % 3 == 2) {
                store.get(store.size() - 2).add(p);
            } else {
                store.get(store.size() - 1).add(p);
            }
        } else {
            if (numberOfRows % 3 == 2) {
                ArrayList<Product> arrayList = new ArrayList<>();
                arrayList.add(p);
                store.add(arrayList);
                numberOfAisles += 1;
                numberOfRows += 1;
            } else {
                ArrayList<Product> arrayList = new ArrayList<>();
                arrayList.add(p);
                store.add(arrayList);
                store.add(null);
                numberOfAisles += 1;
                numberOfRows += 2;
            }
        }
        numberOfProducts += 1;
    }

    public void printStore() {
        for (ArrayList<Product> arrayList : store) {
            printDashes();
            System.out.print("|");
            if (arrayList == null) {
                for (int j = 0; j < numberOfProductsPerAisle * 15 + numberOfProductsPerAisle - 1; j++) {
                    System.out.print(" ");
                }
                System.out.print("|");
            } else {
                int count = 0;
                for (Product product : arrayList) {
                    System.out.printf("%-15s|", product);
                    count++;
                }
                for (int i = count; i < numberOfProductsPerAisle; i++) {
                    System.out.printf("%-15s|", "");
                }
            }
            System.out.println();
        }
        printDashes();
    }

    public void printDashes() {
        int i = numberOfProductsPerAisle * 15 + numberOfProductsPerAisle + 1;
        for (int j = 0; j < i; j++) {
            System.out.print("-");
        }
        System.out.println();
    }
}
